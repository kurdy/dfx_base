import React,{ Fragment } from 'react';
import { useLocation,useNavigate } from 'react-router-dom';
import {removeLocale,setLocale,defaultLocale} from '../services/i18n';

/*
  Select a language using request parameter
  Examples:
  - http://localhost:8080#/?lang=remove
  - http://localhost:8080#/?lang=fr
*/

interface LanguageProps {
  onLanguageChange?: (lang : string) => void
}

const Lang = (Props: LanguageProps) => {
  const navigate = useNavigate();
  const search = useLocation().search;
  const lang = new URLSearchParams(search).get('lang');
  if(lang && lang!=='remove') {
      setLocale(lang).then(()=>{
      if(Props.onLanguageChange) {
        Props.onLanguageChange(lang); 
        navigate('/');
      }
    });
  } else if (lang && lang.toLowerCase()==='remove') {
    removeLocale().then(()=>{
      setLocale(defaultLocale).then(()=>{
        if(Props.onLanguageChange) {
          Props.onLanguageChange(defaultLocale); 
          navigate('/');
        }
      });
    });
  }
  return (
    <Fragment/>
  );
}

export default Lang;