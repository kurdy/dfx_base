import React, { Component, ErrorInfo, ReactNode,Fragment } from "react";

interface Props {
  children: ReactNode;
}

interface State {
    hasError: boolean;
    errorName: string;
    errorInfo: string;
}

class ErrorBoundary extends Component<Props, State> {
  public state: State = {
    hasError: false,
    errorName: '',
    errorInfo: ''
  };

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  public static getDerivedStateFromError(_: Error): State {
    // Update state so the next render will show the fallback UI.
    return { 
        hasError: true,
        errorName: '',
        errorInfo: ''
    };
  }

  public componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    const state = this.state;
    this.setState({
        hasError: state.hasError,
        errorName: error.toString(),
        errorInfo: errorInfo.toString(),
    });
    console.error("Uncaught error:", error, errorInfo);
  }

  public render() {
    if (this.state.hasError) {
        return (
            <Fragment>
                <h1>Unexpected application crash !</h1>
                <h2>Catch error boundary Error infomation:</h2>
                <p>Name: <span/>{this.state.errorName}</p>
                <p>Message: <span/>{this.state.errorInfo}</p>
            </Fragment> 
        );
    }

    return this.props.children;
  }
}

export default ErrorBoundary;