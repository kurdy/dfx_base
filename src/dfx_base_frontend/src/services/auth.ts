import { createActor,canisterId  } from '../../../declarations/dfx_base_backend';
import { AuthClient } from "@dfinity/auth-client";
//import { _SERVICE } from "../../../declarations/dfx_base_backend/dfx_base_backend.did";

const ONE_DAY = BigInt(24 * 60 * 60 * 1000 * 1000 * 1000);

export const whoami = async () : Promise<string> => {
  const authClient = await AuthClient.create();
  const identity = await authClient.getIdentity();
  console.debug("identity: " + JSON.stringify(identity));
  const actor = createActor(canisterId, {
      agentOptions: {
      identity,
      },
  });
  console.debug("actor: " + JSON.stringify(actor));
  const id = await actor.whoami();
  return id.toString();
}

export const isAutenticated = async () : Promise<boolean> => {
  let isAutenticated = false;
  const authClient = await AuthClient.create();
  isAutenticated = await authClient.isAuthenticated();
  return isAutenticated;
}

export const logout = async () : Promise<void> => {
  const authClient = await AuthClient.create();
  authClient.logout();
}

export const login = async (callback? : () => void) => {
  let url = "https://identity.ic0.app"; 
  if(process.env.LOCAL_II_CANISTER) {
    url = process.env.LOCAL_II_CANISTER;
  }
  console.info(`url II : ${url}`);
  const authClient = await AuthClient.create();
  const fCb = callback ? callback : handleLogin;
  await authClient.login({
      maxTimeToLive: ONE_DAY,
      onSuccess: async () => {
        fCb();
      },
      identityProvider: url
    });

}

const handleLogin = () => {
  console.info("Sign in successful");
}
