import React,{Fragment, useEffect, useState} from 'react';
import { HashRouter as Router, Route,Routes} from 'react-router-dom'
import { IntlProvider,FormattedMessage} from 'react-intl';
import {defaultLocale,defaultMessages,getMessages,locales,getLocale,setLocale} from './services/i18n'
import Dfinity from './pages/Dfinity';
import Lang from './components/lang';

import "../assets/main.css";

const NotFound = () => {
  return(
    <div className='center'><FormattedMessage defaultMessage='Route not found' id='key.component.notfound'/></div>
  );
}

const buildInfo = async () => {
  console.info(`PACKAGE_VERSION: ${process.env.PACKAGE_VERSION}`);
  console.info(`PACKAGE_LICENCE: ${process.env.PACKAGE_LICENCE}`);
  console.info(`PACKAGE_NAME: ${process.env.PACKAGE_NAME}`);
  console.info(`BUILD_DATE: ${new Date(Number.parseInt(process.env.BUILD_DATE||Date.now().toString()))}`);
  console.info(`DFX_VERSION: ${process.env.DFX_VERSION}`);
}

let once = false;

const App = () => {
  const [language,setLanguage] = useState(defaultLocale);
  const [messages,setMessages] = useState(defaultMessages);

  useEffect(() => {
    if(!once) {
      once = true;
      buildInfo();  
    }

    const lang = getLocale();
    if (locales.some(i => i === lang)) {
      console.info(`set locale = ${lang}`)
      setLanguage(lang);
      getMessages(lang).then(m=>setMessages(m));
      setLocale(lang);
    }
  },[language])

  return (
    <Fragment>
      <IntlProvider messages={messages} locale={language} defaultLocale={defaultLocale}>
        <Router>
          <Lang onLanguageChange={setLanguage}/>
          <Routes> 
            <Route path="/" element={<Dfinity/>}/>
            <Route path="*" element={<NotFound/>}/>
          </Routes>
        </Router>
      </IntlProvider> 
    </Fragment>
  );
}

export default App;