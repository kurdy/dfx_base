# dfx_base

Base of a dfx project that includes: 
* reactjs
* react-router (HashRouter)
* react-intl
* typescript
* internet identity
* esLint

The idea is to save time when starting a project by including shared libraries and configurations for the above technologies.
You can either make a clone and modify the references to the project name or do it step by step. 

## Prerequisites

* dfx version 0.15.1
    * Install
        * `sh -ci "$(curl -fsSL https://internetcomputer.org/install.sh)"` latest version
        * `DFX_VERSION=0.15.1; sh -ci "$(curl -sSL https://internetcomputer.org/install.sh)"` force the version
    * [dfx sdk cli references](https://internetcomputer.org/docs/current/references/cli-reference/)
* npm
* Internet Identity for local dev. [github internet-identity using-dev-build](https://github.com/dfinity/internet-identity/tree/main/demos/using-dev-build)
    * HowTo [github Auth-Client Demo](https://github.com/krpeacock/auth-client-demo)
    * > **Note** use .env file to define Internet Identity local canister *LOCAL_II_CANISTER* 

## Howto

### Try using gipod

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/kurdy/dfx_base)

### using clone

1. `git clone https://gitlab.com/kurdy/dfx_base.git <my_app>`
1. edit files below and find/replace *dfx_base* by your projet name
    * dfx.json
    * webpack.config.js
    * tsconfig.json
    * package.json
    * modify import in Dfinity.tsx, auth.ts
1. rename directories using your projet name
    * dfx_base_backend
    * dfx_base_frontend
1. run the projet locally

### step by step

1. Create env var with your project name `export PRJ_NAME='dfx_base'`
1. Create project `dfx new $PRJ_NAME`
1. `cd $$PRJ_NAME`
1. Get *tsconfig.json* `wget -O tsconfig.json "https://gitlab.com/kurdy/dfx_base/-/raw/main/tsconfig.json?inline=false"`
1. get *.eslintrc.js*
`wget -O .eslintrc.js "https://gitlab.com/kurdy/dfx_base/-/raw/main/.eslintrc.js?inline=false"`
1. Create a project structure
```bash
mkdir \
    ./src/${PRJ_NAME}_frontend/src/types \
    ./src/${PRJ_NAME}_frontend/src/pages \
    ./src/${PRJ_NAME}_frontend/src/components \
    ./src/${PRJ_NAME}_frontend/src/themes \
    ./src/${PRJ_NAME}_frontend/src/auth \
    ./src/${PRJ_NAME}_frontend/src/services
```
7. Add librairies 
```bash
npm i --save-dev typescript ts-loader css-loader style-loader @types/node @types/react @types/react-dom @types/react-router-dom eslint eslint-plugin-react eslint-plugin-react-hooks @typescript-eslint/parser @typescript-eslint/eslint-plugin dotenv-webpack;
```
```bash
npm i --save react react-dom react-router-dom react-intl @dfinity/auth-client;
```
8. copy files from _./src/dfx_base_frontend_ into your frontend dir
1. copy files from _./src/dfx_base_backend_ into your backend dir
1. run the projet locally


## Running the project locally

If you want to test your project locally, you can use the following commands:

```bash
# Starts the replica, running in the background
dfx start --background --clean

# Deploys your canisters to the replica and generates your candid interface
dfx deploy
```

Once the job completes, your application will be available at `http://localhost:4943?canisterId={asset_canister_id}`.

Additionally, if you are making frontend changes, you can start a development server with

```bash
npm start
```

Which will start a server at `http://localhost:8080`, proxying API requests to the replica at port 4943.

## References
* [ReactJS Typescript Motoko Boilerplate + Authentication by gabrielnic](https://github.com/gabrielnic/dfinity-react)
* [Auth-Client Demo by Kyle Peacock](https://github.com/krpeacock/auth-client-demo)
* [Developpers docs](https://internetcomputer.org/docs/current/developer-docs/quickstart/hello10mins/)
* [Forum](https://forum.dfinity.org/)
* [My previous template without typescript and Internet Identity](https://github.com/rbolog/dfinity_reactJs_reactRouter_babel)